# service-catalog-app

Service Catalog UI is a dashboard built on top of the [service-catalog.yml](https://gitlab.com/gitlab-com/runbooks/blob/master/services/service-catalog.yml), a centralized catalog of our services, and provides capabilities such as navigation, search and sort. We no longer need to jump to various places to find information relating to a service. The dashboard aims to gather all of the information in one place.

### Implementation
The dashboard is written in Go and Golang template is used for rendering. 

### Limitation
1. As of right now, the dashboard reads `service-catalog.yml` data from a local copy. A future iteration will include logic around directly pulling the data from the repository instead of a local copy. 
2. The `teams.yml` is also read from a local copy for now. This file is necessary to provide an illustration of a team. Similar to the above, a future iteration will make use of the `teams.yml`

### Run It 
```
git clone git@gitlab.com:gitlab-com/gl-infra/service-catalog-app.git
cd ui
GO111MODULE=on go run cmd/main.go
```

### Deploy it to Google Cloud Function
```
gcloud beta --project gitlab-infra-automation-stg functions deploy ui --entry-point UI --runtime go111 --trigger-http --set-env-vars="BASE_PATH=/ui"
```

### License information
A free admin template called [Gentelella](https://puikinsh.github.io/gentelella/) is used for the dashboard to speed up the work of visualization. Colorlib is the original author of the template and Gentelella is licensed under the MIT License (MIT).