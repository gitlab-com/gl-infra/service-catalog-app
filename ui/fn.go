package ui

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-com/gl-infra/service-catalog-app/ui/catalog"
)

// Handler is an http.Handler
var Handler = handler()

func getBasePath() string {
	v, ok := os.LookupEnv("BASE_PATH")
	if ok {
		return v
	}

	return ""
}

func getCatalog() (*catalog.ServiceCatalog, error) {
	// For now, we just load the catalog from a local file,
	// in future we'll load this directly from the repo
	return catalog.Parse("service-catalog.yml")
}

func getGitLabTeams() (*catalog.GitLabTeams, error) {
	return catalog.ParseGitLabTeams("team.yml")
}

func svgFunction(name string) (template.HTML, error) {
	svg, err := ioutil.ReadFile("./assets/icons/" + name + ".svg")
	if err != nil {
		return "", err
	}

	return template.HTML(string(svg)), nil
}

func handler() http.Handler {
	e := gin.New()
	e.Use(gin.Recovery())

	e.Static("/assets", "./assets")
	e.SetFuncMap(template.FuncMap{
		"svg": svgFunction,
	})
	e.LoadHTMLGlob("templates/*")

	e.GET("/", func(c *gin.Context) {
		cat, err := getCatalog()
		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		c.HTML(http.StatusOK, "index.html", gin.H{
			"basePath": getBasePath(),
			"catalog":  cat,
		})
	})

	e.GET("/services", func(c *gin.Context) {
		cat, err := getCatalog()
		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		c.HTML(http.StatusOK, "services.html", gin.H{
			"basePath": getBasePath(),
			"catalog":  cat,
		})
	})

	e.GET("/teams", func(c *gin.Context) {
		cat, err := getCatalog()
		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		c.HTML(http.StatusOK, "teams.html", gin.H{
			"basePath": getBasePath(),
			"catalog":  cat,
		})
	})

	e.GET("/services/:name", func(c *gin.Context) {
		cat, err := getCatalog()

		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		service := cat.FindService(c.Param("name"))
		if service == nil {
			c.AbortWithError(404, fmt.Errorf("Service %v not found", c.Param("name")))
			return
		}

		c.HTML(http.StatusOK, "service.html", gin.H{
			"basePath": getBasePath(),
			"service":  service,
		})
	})

	e.GET("/teams/:name", func(c *gin.Context) {
		cat, err := getCatalog()
		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		serviceTeam := cat.FindTeam(c.Param("name"))
		if serviceTeam == nil {
			c.AbortWithError(404, fmt.Errorf("Team %v not found", c.Param("name")))
			return
		}

		teams, err := getGitLabTeams()
		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		chain := teams.FindDirectsBySlug(serviceTeam.ManagerSlug)
		if chain == nil {
			c.AbortWithError(404, fmt.Errorf("Directs reporting to slug %v not found", serviceTeam.ManagerSlug))
			return
		}

		c.HTML(http.StatusOK, "team.html", gin.H{
			"basePath": getBasePath(),
			"managerSlug":  serviceTeam.ManagerSlug,
			"chain":  chain,
			"team": serviceTeam,
		})
	})

	return e
}

// UI is the app
func UI(w http.ResponseWriter, r *http.Request) {
	Handler.ServeHTTP(w, r)
}
