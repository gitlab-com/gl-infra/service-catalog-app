package catalog

func (c *ServiceCatalog) FindService(name string) *Service {
	for _, v := range c.Services {
		if v.Name == name {
			return &v
		}
	}
	return nil
}

func (c *ServiceCatalog) teamMap() map[string]Team {
	result := make(map[string]Team, len(c.Teams))
	for _, v := range c.Teams {
		result[v.Name] = v
	}
	return result
}

func (c *ServiceCatalog) FindTeam(name string) *Team {
	for _, v := range c.Teams {
		if v.Name == name {
			return &v
		}
	}
	return nil
}

func (c *GitLabTeams) FindGitLabTeam(slug string) *GitLabTeam {
	for _, v := range c.GitLabTeams {
		if v.GitLabTeamYAML.Slug == slug {
			return &v
		}
	}
	return nil
}

func (c *GitLabTeams) FindDirectsBySlug(slug string) []GitLabTeam {
	var chain []GitLabTeam

	for _, v := range c.GitLabTeams {
		if v.GitLabTeamYAML.ReportsTo == slug || v.GitLabTeamYAML.Slug == slug {
			chain = append(chain, v)
		} 
	}
	return chain
}

func (c *ServiceCatalog) tierMap() map[string]Tier {
	result := make(map[string]Tier, len(c.Tiers))
	for _, v := range c.Tiers {
		result[v.Name] = v
	}
	return result
}
